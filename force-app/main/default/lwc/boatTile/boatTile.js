import { LightningElement, api, wire } from 'lwc';

const TILE_WRAPPER_SELECTED_CLASS = 'tile-wrapper selected';
const TILE_WRAPPER_UNSELECTED_CLASS  = 'tile-wrapper';

export default class BoatTile extends LightningElement {

    @api boat;
    @api selectedBoatId = '';

    // Getter for dynamically setting the background image for the picture
    get backgroundStyle(){
        return `background-image:url(${this.boat.Picture__c})`;
    }

    get tileClass() {
        return (this.boat.Id === this.selectedBoatId ? TILE_WRAPPER_SELECTED_CLASS : TILE_WRAPPER_UNSELECTED_CLASS)
    }

    // Fires event with the Id of the boat that has been selected.
    selectBoat(){
        console.log('selectBoat ');
        this.selectedBoatId = this.boat.Id;
        let selectedBoatObj = { boatId : this.boat.Id };
        const boatSelectEvent = new CustomEvent('boatselect', { detail: selectedBoatObj });
        this.dispatchEvent(boatSelectEvent);
    }
}