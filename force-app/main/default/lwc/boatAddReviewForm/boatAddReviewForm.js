import { api, LightningElement } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent'; // Import platformShowToastEvent to utilize toast notifications
import BOAT_REVIEW_OBJECT  from '@salesforce/schema/BoatReview__c'; //import BOAT_REVIEW_OBJECT from schema - BoatReview__c
import NAME_FIELD from '@salesforce/schema/BoatReview__c.Name';
import COMMENT_FIELD from '@salesforce/schema/BoatReview__c.Comment__c';

const SUCCESS_TITLE = 'Review Created!';
const SUCCESS_VARIANT = 'success';

export default class BoatAddReviewForm extends LightningElement {

    boatId;
    rating;
    boatReviewObject = BOAT_REVIEW_OBJECT;
    nameField = NAME_FIELD;
    commentField = COMMENT_FIELD;
    labelSubject = 'Review Subject';
    labelRating = 'Rating';

    // Getter and Setter to allow for logic to run on recordId change
    // this getter must be public
    @api
    get recordId() {
        return this.boatId;
    }
    set recordId(value) {
        this.setAttribute('boatId', value);
        this.boatId = value;
    }

    handleSubmit(event) {
        event.preventDefault();
        const fields = event.detail.fields;
        fields.Rating__c = this.rating;
        fields.Boat__c = this.boatId;
        console.table(fields);
        this.template.querySelector('lightning-record-edit-form').submit(fields);
    }

    handleSuccess(event) {
        this.dispatchEvent(
            new ShowToastEvent({
                title: SUCCESS_TITLE,
                variant: SUCCESS_VARIANT
            })
        );
        
        this.dispatchEvent(new CustomEvent('createreview'));
        this.handleReset();
    }

    handleRatingChanged(event){
        const rating = event.detail.rating;
        console.table(rating);
        this.rating = rating;
    }

    handleReset() {
        const inputFields = this.template.querySelectorAll(
            'lightning-input-field'
        );
        if (inputFields) {
            inputFields.forEach(field => {
                field.reset();
            });
        }
    }

}