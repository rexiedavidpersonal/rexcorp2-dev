import { LightningElement, api } from 'lwc';
import getAllReviews from '@salesforce/apex/BoatDataService.getAllReviews';
import { NavigationMixin } from 'lightning/navigation';

export default class BoatReviews extends NavigationMixin(LightningElement) {

    boatId;
    error;
    boatReviews;
    isLoading = false;

    @api
    get recordId() {
        return this.boatId;
    }

    set recordId(value) {
        this.setAttribute('boatId', value);
        this.boatId = value;
        this.getReviews();
    }

    get reviewsToShow() {
        return this.boatReviews && this.boatReviews.length > 0 ? true : false;
    }

    @api
    refresh() {
        console.log('refresh');
        this.getReviews();
    }

    getReviews() {
        this.isLoading = true;
        getAllReviews({ boatId: this.boatId })
            .then(result => {
                console.log('getReviews >>> result >>> ',JSON.stringify(result));
                this.boatReviews = result;
                this.isLoading = false;
            })
            .catch(error => {
                console.log('getReviews >>> error');
                this.error = error;
                this.boatReviews = undefined;
                this.isLoading = false;
            })
    }

    // Helper method to use NavigationMixin to navigate to a given record on click
    navigateToRecord(event) {
        const userId = event.target.dataset.recordId;
        console.log('navigateToRecord >>> ', userId);
        // Generate a URL to a User record page
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: userId,
                objectApiName: 'User',
                actionName: 'view',
            },
        });
    }
}