import { LightningElement, wire, api } from 'lwc';
import getBoatsByLocation from '@salesforce/apex/BoatDataService.getBoatsByLocation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent'; // Import platformShowToastEvent to utilize toast notifications

const LABEL_YOU_ARE_HERE = 'You are here!';
const ICON_STANDARD_USER = 'standard:user';
const ERROR_TITLE = 'Error loading Boats Near Me';
const ERROR_VARIANT = 'error';

export default class BoatsNearMe extends LightningElement {
    @api boatTypeId;
    mapMarkers = [];
    isLoading = true;
    isRendered = false;
    latitude;
    longitude;

    // Add the wired method from the Apex Class
    // Name it getBoatsByLocation, and use latitude, longitude and boatTypeId
    // Handle the result and calls createMapMarkers
    @wire(getBoatsByLocation, { latitude: '$latitude', longitude: '$longitude', boatTypeId: '$boatTypeId' })
    wiredBoatsJSON({ error, data }) {
        if (data) {
            this.isLoading = true;
            console.log('boatsNearMe >>> wiredBoatsJSON >>> data >>> ', JSON.stringify(data, null, 2));
            this.createMapMarkers(JSON.parse(data));
        }
        if (error) {
            console.log('boatsNearMe >>> wiredBoatsJSON >>> error >>> ', JSON.stringify(error, null, 2));
            this.showErrorToast(error.body.message);
            const evt = new ShowToastEvent({
                title: ERROR_TITLE,
                message: error.body.message,
                variant: ERROR_VARIANT,
                mode: 'dismissable'
            });
            this.dispatchEvent(evt);
            this.isLoading = false;
        }
    }

    // Controls the isRendered property
    // Calls getLocationFromBrowser()
    renderedCallback() {
        if (this.isRendered) {
            return;   
        }
        this.isRendered = true;
        console.log('boatsNearMe >>> renderedCallback ');
        this.getLocationFromBrowser();
        
    }

    // Gets the location from the Browser
    // position => {latitude and longitude}
    getLocationFromBrowser() {
        console.log('boatsNearMe >>> getLocationFromBrowser ');
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(position => {
                // Get the Latitude and Longitude from Geolocation API
                this.latitude = position.coords.latitude;
                this.longitude = position.coords.longitude;
            })
        }
    }

    // Creates the map markers
    createMapMarkers(boatData) {
        const newMarkers = boatData.map(boat => {
            return {
                location: {
                    Latitude: boat.Geolocation__Latitude__s, 
                    Longitude: boat.Geolocation__Longitude__s 
                },
                title: boat.Name
            }
        });
        console.log("boatsNearMe >>> createMapMarkers >>> newMarkers ", newMarkers);
        newMarkers.unshift({
            location: { Latitude: this.latitude, Longitude: this.longitude },
            title: LABEL_YOU_ARE_HERE,
            icon: ICON_STANDARD_USER 
        });
        this.mapMarkers = newMarkers;
        this.isLoading = false;
    }

    // showErrorToast(message) {
    //     const evt = new ShowToastEvent({
    //         title: ERROR_TITLE,
    //         message: message,
    //         variant: ERROR_VARIANT,
    //         mode: 'dismissable'
    //     });
    //     this.dispatchEvent(evt);
    // }
}