import { LightningElement, wire, api } from 'lwc';
import { getRecord, getFieldValue, updateRecord, getRecordNotifyChange } from 'lightning/uiRecordApi'; // Import uiRecordApi to retrieve and update records
import { ShowToastEvent } from 'lightning/platformShowToastEvent'; // Import platformShowToastEvent to utilize toast notifications
import getBoats from '@salesforce/apex/BoatDataService.getBoats'; // Import getBoats method from BoatDataService class
import updateBoatList from '@salesforce/apex/BoatDataService.updateBoatList'; // Import updateBoatList method from BoatDataService class
import { publish, MessageContext } from 'lightning/messageService'; // Import message service features required for publishing and the message channel
import { refreshApex } from '@salesforce/apex';
import BoatMC from '@salesforce/messageChannel/BoatMessageChannel__c';

const SUCCESS_TITLE = 'Success';
const MESSAGE_SHIP_IT = 'Ship it!';
const SUCCESS_VARIANT = 'success';
const ERROR_TITLE = 'Error';
const ERROR_VARIANT = 'error';

export default class BoatSearchResults extends LightningElement {
    selectedBoatId;
    boatTypeId = '';
    boats;
    error;
    isLoading = false;
    draftValues = [];
    wiredResults;
    // Declare Table Columns
    columns = [
        { label: 'Name', fieldName: 'Name', type: 'text', editable: 'true' },
        { label: 'Length', fieldName: 'Length__c', type: 'number', editable: 'true' },
        { label: 'Price', fieldName: 'Price__c', type: 'currency', editable: 'true' },
        { label: 'Description', fieldName: 'Description__c', type: 'text', editable: 'true' }
    ];

    // wired message context
    @wire(MessageContext)
    messageContext;

    // wired getBoats method  
    @wire(getBoats, { boatTypeId: '$boatTypeId' })
    wiredBoats(result) {
        this.wiredResults = result;
        if(result.data){
            console.log(JSON.stringify(result.data, null, 2));
            this.boats = result.data;
            this.error = undefined;
            this.isLoading = false;
            this.notifyLoading(this.isLoading);
        }   else if (result.error) {
            console.error(JSON.stringify(result.error, null, 2));
            this.boats = undefined;
            this.error = result.error;
            this.isLoading = false;
            this.notifyLoading(this.isLoading);
        }
        
    // wiredBoats({ error, data }) {
        // if (data) {
            // console.log(JSON.stringify(data, null, 2));
            // this.boats = data;
            // this.error = undefined;
            // this.isLoading = false;
            // this.notifyLoading(this.isLoading);
        // } else if (error) {
        //     console.error(JSON.stringify(error, null, 2));
        //     this.boats = undefined;
        //     this.error = error;
        //     this.isLoading = false;
        //     this.notifyLoading(this.isLoading);
        // }
    }

    // public function that updates the existing boatTypeId property
    // uses notifyLoading
    @api searchBoats(boatTypeId) {
        this.isLoading = true;
        this.notifyLoading(this.isLoading);
        console.log('searchBoats => boatTypeId => ', boatTypeId);
        this.boatTypeId = boatTypeId;
    }

    // this public function must refresh the boats asynchronously
    // uses notifyLoading
    @api
    refresh() {
        this.isLoading = true;
        this.notifyLoading(this.isLoading);      
        // refreshApex(this.boats);
        this.draftValues = [];
        this.isLoading = false;
        this.notifyLoading(this.isLoading); 
        return refreshApex(this.wiredResults); 
        
        // this.isLoading = true;
        // this.notifyLoading(this.isLoading);
        // // Display fresh data in the datatable
        // await refreshApex(this.boats);
        // this.isLoading = false;
        // console.log('refresh');
        // this.notifyLoading(this.isLoading);
        // await refreshApex(this.boats).then(() => {
        //     // Clear all draft values in the datatable
        //     this.draftValues = [];
        // });
    }

    // this function must update selectedBoatId and call sendMessageService
    updateSelectedTile(event) {
        this.selectedBoatId = event.detail.boatId;
        this.sendMessageService(this.selectedBoatId);
    }

    // Publishes the selected boat Id on the BoatMC.
    sendMessageService(boatId) {
        publish(this.messageContext, BoatMC, { recordId: boatId });
        // explicitly pass boatId to the parameter recordId
    }

    // The handleSave method must save the changes in the Boat Editor
    // passing the updated fields from draftValues to the 
    // Apex method updateBoatList(Object data).
    // Show a toast message with the title
    // clear lightning-datatable draft values
    // handleSave(event) {
    //     const recordInputs = event.detail.draftValues.slice().map(draft => {
    //         const fields = Object.assign({}, draft);
    //         return { fields };
    //     });

    //     const promises = recordInputs.map(recordInput => updateRecord(recordInput));

    //     Promise.all(promises)
    //         .then(() => {
    //             // this.showToastEvent('Success', 'Ship It!', 'success');
    //             this.dispatchEvent(
    //                 new ShowToastEvent({
    //                     title: SUCCESS_TITLE,
    //                     message: MESSAGE_SHIP_IT,
    //                     variant: SUCCESS_VARIANT
    //                 })
    //             );
    //             this.draftValues = [];
    //             this.refresh();
    //             console.log('handleSave >>> refresh');
    //         })
    //         .catch(error => {
    //             this.error = error;
    //             this.dispatchEvent(
    //                 new ShowToastEvent({
    //                     title: ERROR_TITLE,
    //                     message: error.body.message,
    //                     variant: ERROR_VARIANT
    //                 })
    //             );
    //         }).finally(() => {
                
    //         });
    // }
    async handleSave(event) {
        // notify loading
        this.notifyLoading(true);
        const updatedFields = event.detail.draftValues;

        // Prepare the record IDs for getRecordNotifyChange()
        const notifyChangeIds = updatedFields.map(row => { return { "recordId": row.Id } });

        console.log('notifyChangeIds >>> ',JSON.stringify(notifyChangeIds));

        // Pass edited fields to the updateContacts Apex controller
        await updateBoatList({ data: updatedFields })
            .then(result => {
                console.log(JSON.stringify("Apex update result: " + result));
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: SUCCESS_TITLE,
                        message: MESSAGE_SHIP_IT,
                        variant: SUCCESS_VARIANT
                    })
                );
                // Refresh LDS cache and wires
                getRecordNotifyChange(notifyChangeIds);
                // Display fresh data in the datatable
                this.refresh();
                // this.isLoading = true;
                // this.notifyLoading(this.isLoading);      
                // refreshApex(this.wiredBoats).then(() => {
                //     // Clear all draft values in the datatable
                //     this.draftValues = [];
                //     this.isLoading = false;
                //     this.notifyLoading(this.isLoading); 
                // });
            }).catch(error => {
                console.log(JSON.stringify("error: " + error));
                const errMessage = (error.body.message)  ? error.body.message : 'Default Error message.'
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: ERROR_TITLE,
                        message: errMessage,
                        variant: ERROR_VARIANT
                    })
                );
            }).finally(() => { });;
    }

    notifyLoading(isLoading) {
        var spinnerEvent;
        if (isLoading) {
            spinnerEvent = new CustomEvent('loading');
        } else {
            spinnerEvent = new CustomEvent('doneloading');
        }
        this.dispatchEvent(spinnerEvent);
    }
}