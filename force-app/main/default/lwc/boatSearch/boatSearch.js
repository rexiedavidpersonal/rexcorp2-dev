import { LightningElement } from 'lwc';
import { publish, MessageContext } from 'lightning/messageService'; // Import message service features required for publishing and the message channel
import BOATMC from '@salesforce/messageChannel/BoatMessageChannel__c'; // Import BoatMessageChannel message channels 
import { NavigationMixin } from 'lightning/navigation';

export default class BoatSearch extends  NavigationMixin (LightningElement) {

    isLoading = false;

    // Handles loading event
    handleLoading(){
        this.isLoading = true;
    }

    handleDoneLoading(){
        this.isLoading = false;
    }

    // Handles search boat event
    // This custom event comes from the form
    searchBoats(event){
        console.log('searchBoats ', JSON.stringify(event.detail));
        this.template.querySelector("c-boat-search-results").searchBoats(event.detail.boatTypeId);
    }

    createNewBoat(){
        console.log('Create new boat');
        this[NavigationMixin.Navigate]({
            type: 'standard__objectPage',
            attributes: {
                objectApiName: 'Boat__c',
                actionName: 'new'
            }
        });
    }

}